#include <Wire.h>

#define PHOTO_RES A0
#define SLAVE_ADDRES 9
#define RESPONSE_SIZE 1

const int STEP = 10;
const int DIFF = 10;
const int ARCHIVE_FACTOR = 5;

bool is_calibration = false;
bool is_report_sended = false;
bool data_send_aborted = false;
bool showData = false;
bool minRangeChecked = false;
bool maxRangeChecked = false;
int minRange = -1;
int maxRange = -1;

int previous = -1;
int cntr = 0;
int records_to_do = 0;

const int epsilon = 3;


int getMyPhotoResValue(){
  return analogRead(PHOTO_RES);
}

void setup() {
  pinMode(PHOTO_RES, INPUT);
  Serial.begin(9600);
  Serial.println("i'm Master");
  Wire.begin(D1, D2);
}

void loop() {
   if (showData) {
      Serial.print("value: ");
      Serial.println(getMyPhotoResValue());
    }

  if (Serial.available()) {
    char cmd = Serial.read();

    if (cmd == 's') {
      showData = !showData;
    }

    if (cmd == 'c') {
      Serial.println("'o' for confirmation and turn off the light");
    }

    if (cmd == 'r'){
      is_calibration = false;
      is_report_sended = false;
      data_send_aborted = false;
      minRange = -1;
      maxRange = -1;
      maxRangeChecked = false;
      minRangeChecked = false;
      previous = -1;
      cntr = 0;
      records_to_do = 0;
      Serial.println("resetted by Master");
    }

    if (cmd == 'o') {
      if (minRange == -1) {
        minRange = getMyPhotoResValue() + DIFF;
        Serial.println("'o' for confirmation and turn on the light");
      } else if (maxRange == -1) {
        maxRange = getMyPhotoResValue() - DIFF;
        is_calibration = true;

        Wire.beginTransmission(SLAVE_ADDRES);
        Wire.write('r');
        records_to_do  = (int)((maxRange - minRange + 1) / STEP);
        Wire.write(records_to_do);
        Serial.print("records to do: ");
        Serial.println(records_to_do);
        Wire.endTransmission(SLAVE_ADDRES);

        Serial.println(minRange);
        Serial.println(maxRange);
        Serial.println("calibration has started, turning off the light and slowly add more light every time");
        delay(5000);
      }
    }
  }

  if (is_calibration) {
    Wire.beginTransmission(SLAVE_ADDRES);

    if (!is_report_sended) {
      Wire.write("c");
      is_report_sended = true;
    }

    if (is_report_sended && !data_send_aborted){
      int res = getMyPhotoResValue();

      if (res <= minRange && !minRangeChecked) {
        minRangeChecked = true;
      }

      if (res >= minRange && !maxRangeChecked) {
        maxRangeChecked = true;
      }

      if (res - previous >= STEP){
        Serial.print("value for calibration: ");
        Serial.println(getMyPhotoResValue());
        Wire.write((int)(res / ARCHIVE_FACTOR));
        cntr += 1;
        previous = res;
      }

      Wire.endTransmission(SLAVE_ADDRES);

      if (previous == -1){
        previous = res;
      }
    }

    if (minRangeChecked && maxRangeChecked && cntr >= records_to_do ) {
      is_calibration=false;
      is_report_sended = false;
      data_send_aborted = false;

      minRange = -1;
      maxRange = -1;

      Wire.beginTransmission(SLAVE_ADDRES);
      Wire.write('s');
      Wire.endTransmission(SLAVE_ADDRES);
      Serial.println("calibration finished");
    }
  }
}
