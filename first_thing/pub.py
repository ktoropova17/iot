import time
import paho.mqtt.client as paho
import random

broker = "broker.emqx.io"

client = paho.Client("client-isu-741")

print("Connecting to broker", broker)
client.connect(broker)
client.loop_start()
print("Publishing")

for _ in range(10):
    state = "u" if random.randint(0, 1) else "d"
    print(f'state is {state}')
    client.publish("heronwater", state)
    time.sleep(random.randint(4, 10))

client.disconnect()
client.loop_stop()