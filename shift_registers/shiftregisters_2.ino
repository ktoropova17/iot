int latchPin = 5;      
int clockPin = 3;
int dataPin = 7;      

// 2 3 4 5 6 7 8 9 
// A B . C D E G F
byte digits[10] = {
  B11011101,  // 0
  B01010000,  // 1
  B11001110,  // 2
  B11011010,  // 3
  B01010011,  // 4
  B10011011,  // 5
  B10111111,  // 6
  B11010000,  // 7
  B11011111,  // 8
  B11111011   // 9
};


bool flag{false};
int time_digits[4]{};
unsigned long previous_millis{0};
const long interval{1000};

void setup(){
  Serial.begin(9600);
  pinMode(latchPin, OUTPUT);
  digitalWrite(latchPin, LOW);
  pinMode(clockPin, OUTPUT);
  digitalWrite(clockPin, LOW);
  pinMode(dataPin, OUTPUT);
  digitalWrite(dataPin, LOW);
}

void loop(){
  if (!flag && Serial.available() >= 4) {
    for(int i = 0; i < 4; i++) {
     while(Serial.available() <= 0){}
     time_digits[i] = (int)(Serial.read() - '0');
    }
    flag = true;
  }
  if (flag && (millis() - previous_millis >= interval)) {
    previous_millis = millis();
    increaseTimeDigits();
    displayTimeDigits();
  }
}

void increaseTimeDigits(){
  for(int i = 3; i >= 0; i--){
    time_digits[i] = (time_digits[i] + 1) % 10;
    if (time_digits[i] != 0){
      break;
    }
  }
}

void displayTimeDigits(){
  show_number(time_digits[0]);
  show_number(time_digits[1]);
  show_number(time_digits[2]);
  show_number(time_digits[3]);
}

void show_number(int digit)
{
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, digits[digit]); 
  digitalWrite(latchPin, HIGH);
}